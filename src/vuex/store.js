import Vue from 'vue'
import Vuex from 'vuex'
import Immutable from 'immutable'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    landPlayed: undefined,
    landTypes: Immutable.Set.of('Mountain', 'Island', 'Forest', 'Swamp', 'Plains'),
    landsPlayed: Immutable.List()
  },
  mutations: {
    playLand (state, _landPlayed) {
      state.landTypes = state.landTypes.add(_landPlayed)
      state.landsPlayed = state.landsPlayed.push(_landPlayed)
      state.landPlayed = _landPlayed
    },
    untap (state) {
      state.landPlayed = undefined
    }
  }
})
